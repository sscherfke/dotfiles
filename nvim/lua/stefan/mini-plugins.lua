-- Stuff that should probably be broken out into plugins, but hasn't proved to
-- be worth the time to do so just yet.

-- SmartHome (vim tip 315) {{{

local function SmartHome()
  local col = vim.fn.col(".")
  vim.cmd("normal! ^")
  if col == vim.fn.col(".") then
    vim.cmd("normal! 0")
  end
end
vim.keymap.set("n", "0", SmartHome, { silent = true })
vim.keymap.set("n", "<Home>", SmartHome, { silent = true })
vim.keymap.set("i", "<Home>", SmartHome, { silent = true })

-- }}}

-- Synstack {{{

-- Show the stack of syntax highlighting classes affecting whatever is under the
-- cursor.
local function SynStack()
  vim.cmd([[echo join(map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")'), " > ")]])
end

vim.keymap.set("n", "<leader>hi", SynStack)

-- }}}

-- Highlight Word {{{
--
-- This mini-plugin provides a few mappings for highlighting words temporarily.
--
-- Sometimes you're looking at a hairy piece of code and would like a certain
-- word or two to stand out temporarily.  You can search for it, but that only
-- gives you one color of highlighting.  Now you can use <leader>N where N is
-- a number from 1-6 to highlight the current word in a specific color.

local function _hiInterestingWord(n)
  -- Get word ("w") under cursor
  local word = vim.fn.expand("<cword>")
  -- Calculate an arbitrary match ID.  Hopefully nothing else is using it.
  local match_id = 86750 + n
  -- Clear existing matches, but don't worry if they don't exist.
  pcall(vim.fn.matchdelete, match_id)
  -- Construct a literal pattern that has to match at boundaries.
  local pattern = "\\V\\<" .. vim.fn.escape(word, "\\") .. "\\>"
  -- Actually match the words.
  vim.fn.matchadd("InterestingWord" .. n, pattern, 1, match_id)
end
local function HiInterestingWord(n)
  return function()
    _hiInterestingWord(n)
  end
end
function HiInterestingWordClear()
  for i = 1, 6 do
    local match_id = 86750 + i
    pcall(vim.fn.matchdelete, match_id)
  end
end

for i = 1, 6 do
  vim.keymap.set("n", "<leader>" .. i, HiInterestingWord(i), { silent = true })
end
