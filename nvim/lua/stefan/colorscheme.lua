local namespace = "stefan"
local colorscheme = "stylo"

-- Actually set colorscheme
local ok, os_mode
ok, os_mode = pcall(vim.fn.systemlist, "dm get")
if ok then
  vim.opt.background = os_mode[1]
end
ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end

-- "opt.background" detects the current system dark/light mode.
-- Don't set it explicitly!
--
-- Detect OS dark/light mode by running the following function every 2s:
local function check_is_dark_mode()
  vim.fn.jobstart({ "dm", "get" }, {
    stdout_buffered = true,
    on_stdout = function(_, data, _)
      local os_mode = table.concat(data)
      if vim.opt.background:get() ~= os_mode then
        vim.opt.background = os_mode
      end
    end,
  })
end
local timer = vim.loop.new_timer()
timer:start(0, 2000, vim.schedule_wrap(check_is_dark_mode))

-- Reload the colorscheme whenever we write the file.
local au_color_dev = vim.api.nvim_create_augroup("color_dev", { clear = true })
vim.api.nvim_create_autocmd("BufWritePost", {
  pattern = colorscheme .. ".lua",
  group = au_color_dev,
  callback = function()
    package.loaded[namespace .. "." .. colorscheme] = nil
    vim.cmd("colorscheme " .. colorscheme)
  end,
})
