-- References:
-- - https://jordanelver.co.uk/blog/2015/05/27/working-with-vim-colorschemes/
-- - https://gist.github.com/swarn/fb37d9eefe1bc616c2a7e476c0bc0316
local M = {}

-- Generated color values {{{
local colors = {
  dark = {
    gui = {
      base00 = "#18191A",
      base01 = "#222528",
      base02 = "#353B41",
      base03 = "#48535F",
      base04 = "#68798B",
      base05 = "#96AEC7",
      base06 = "#C8DAEC",
      base07 = "#E4ECF5",
      red = "#CD7B82",
      orange = "#C88F70",
      yellow = "#B29D5A",
      green = "#6EAE8C",
      cyan = "#63ACB0",
      blue = "#70A3D6",
      purple = "#A191D9",
      magenta = "#BE85CB",
      bright_red = "#F34A67",
      bright_orange = "#EE7928",
      bright_yellow = "#CDA712",
      bright_green = "#2DA974",
      bright_cyan = "#1DAAB1",
      bright_blue = "#3395ED",
      bright_purple = "#9369F2",
      bright_magenta = "#BA59D0",
      dim_red = "#80142C",
      dim_orange = "#793807",
      dim_yellow = "#604D05",
      dim_green = "#0A5638",
      dim_cyan = "#04585C",
      dim_blue = "#054C85",
      dim_purple = "#4B288C",
      dim_magenta = "#601E6E",
    },
    cterm = {
      base00 = "234",
      base01 = "0",
      base02 = "8",
      base03 = "239",
      base04 = "243",
      base05 = "110",
      base06 = "7",
      base07 = "15",
      red = "1",
      orange = "137",
      yellow = "3",
      green = "2",
      cyan = "6",
      blue = "4",
      purple = "5",
      magenta = "140",
      bright_red = "9",
      bright_orange = "166",
      bright_yellow = "11",
      bright_green = "10",
      bright_cyan = "14",
      bright_blue = "12",
      bright_purple = "13",
      bright_magenta = "134",
      dim_red = "52",
      dim_orange = "52",
      dim_yellow = "58",
      dim_green = "23",
      dim_cyan = "23",
      dim_blue = "24",
      dim_purple = "54",
      dim_magenta = "53",
    },
  },
  light = {
    gui = {
      base00 = "#FBF6ED",
      base01 = "#ECE8E0",
      base02 = "#D4D4D4",
      base03 = "#C7C7C7",
      base04 = "#919191",
      base05 = "#5E5E5E",
      base06 = "#3C3C3C",
      base07 = "#191919",
      red = "#A43C31",
      orange = "#BA5C00",
      yellow = "#C78800",
      green = "#52751D",
      cyan = "#268389",
      blue = "#2F5F98",
      purple = "#874292",
      magenta = "#A63352",
      bright_red = "#CA4134",
      bright_orange = "#E27415",
      bright_yellow = "#EAA000",
      bright_green = "#618F00",
      bright_cyan = "#0D8D94",
      bright_blue = "#2F78CA",
      bright_purple = "#A54BB4",
      bright_magenta = "#C44164",
      dim_red = "#ECCCC7",
      dim_orange = "#ECCEBC",
      dim_yellow = "#E8D1B1",
      dim_green = "#C8DDB1",
      dim_cyan = "#AEDFE2",
      dim_blue = "#C5D6ED",
      dim_purple = "#E3CCE6",
      dim_magenta = "#EDCBD0",
    },
    cterm = {
      base00 = "231",
      base01 = "0",
      base02 = "8",
      base03 = "251",
      base04 = "246",
      base05 = "59",
      base06 = "7",
      base07 = "15",
      red = "1",
      orange = "130",
      yellow = "3",
      green = "2",
      cyan = "6",
      blue = "4",
      purple = "5",
      magenta = "125",
      bright_red = "9",
      bright_orange = "166",
      bright_yellow = "11",
      bright_green = "10",
      bright_cyan = "14",
      bright_blue = "12",
      bright_purple = "13",
      bright_magenta = "168",
      dim_red = "224",
      dim_orange = "224",
      dim_yellow = "223",
      dim_green = "151",
      dim_cyan = "152",
      dim_blue = "189",
      dim_purple = "225",
      dim_magenta = "224",
    },
  },
}

-- Set background and normal text color
local back = "base00"
local text = "base05"

-- Set neovim embedded terminal colors
local term_colors = {
  term0 = "base01",
  term1 = "red",
  term2 = "green",
  term3 = "yellow",
  term4 = "blue",
  term5 = "purple",
  term6 = "cyan",
  term7 = "base06",
  term8 = "base02",
  term9 = "bright_red",
  term10 = "bright_green",
  term11 = "bright_yellow",
  term12 = "bright_blue",
  term13 = "bright_purple",
  term14 = "bright_cyan",
  term15 = "base07",
}
-- }}} Generated color values

--- Return a table with the proper for the current mode (dark|light)
--- and GUI capabilities (vim.o.termguicolors)
function M.get_colors()
  local mode = vim.o.background
  local gui = vim.o.termguicolors and "gui" or "cterm"
  return colors[mode][gui]
end

--- Load the actual colorscheme and apply highlights
function M.load()
  local blend = require("stefan.util").blend
  local c = M.get_colors()

  vim.api.nvim_command("hi clear")
  if vim.fn.exists("syntax_on") then
    vim.api.nvim_command("syntax reset")
  end

  vim.g.colors_name = "stylo"

  local function hi(group, properties)
    vim.api.nvim_set_hl(0, group, properties)
  end

  if vim.fn.has("gui_running") == 1 then
    c.normal_bg = c[back]
    c.normal_fg = c[text]
  else
    c.normal_bg = nil
    c.normal_fg = nil
  end

  c.bg = c.base00
  c.ui_dim = c.base01
  c.ui = c.base02
  c.ui_bright = c.base03
  c.text_dim = c.base04
  c.text = c.base05
  c.text_bright = c.base06
  c.text_very_bright = c.base07

  c.float_bg = c.bg
  c.float_text = c.text
  c.border = c.text_dim

  c.error = c.bright_red
  c.warning = c.bright_yellow
  c.info = c.bright_blue
  c.debug = c.bright_purple
  c.trace = c.bright_cyan
  c.hint = c.debug

  -- Set neovim embedded terminal colors
  hi("TermColor0", { fg = c[term_colors.term0] })
  hi("TermColor1", { fg = c[term_colors.term1] })
  hi("TermColor2", { fg = c[term_colors.term2] })
  hi("TermColor3", { fg = c[term_colors.term3] })
  hi("TermColor4", { fg = c[term_colors.term4] })
  hi("TermColor5", { fg = c[term_colors.term5] })
  hi("TermColor6", { fg = c[term_colors.term6] })
  hi("TermColor7", { fg = c[term_colors.term7] })
  hi("TermColor8", { fg = c[term_colors.term8] })
  hi("TermColor9", { fg = c[term_colors.term9] })
  hi("TermColor10", { fg = c[term_colors.term10] })
  hi("TermColor11", { fg = c[term_colors.term11] })
  hi("TermColor12", { fg = c[term_colors.term12] })
  hi("TermColor13", { fg = c[term_colors.term13] })
  hi("TermColor14", { fg = c[term_colors.term14] })
  hi("TermColor15", { fg = c[term_colors.term15] })

  -- NOTE: Not all available highlight groups are used here.  See ":help highlight-groups".

  -- General interface

  hi("Normal", { fg = c.normal_fg, bg = c.normal_bg })

  hi("NormalFloat", { fg = c.float_text, bg = c.float_bg })
  hi("FloatBorder", { fg = c.border, bg = c.float_bg })
  hi("FloatTitle", { fg = c.bright_blue, bg = c.float_bg, bold = true })
  hi("MsgArea", { link = "NormalFloat" })
  hi("Pmenu", { fg = c.text, bg = c.ui_dim })
  hi("PmenuSbar", { fg = c.text, bg = c.ui })
  hi("PmenuThumb", { fg = c.text, bg = c.text })
  hi("PmenuSel", { fg = c.ui_dim, bg = c.green })
  hi("WildMenu", { fg = c.ui_dim, bg = c.green })

  hi("Cursor", { fg = c.bg, bg = c.text })
  hi("CursorLineNr", { fg = c.red, bg = c.ui_dim })
  hi("CursorLine", { bg = c.ui_dim })
  hi("CursorColumn", { bg = c.ui_dim })
  hi("ColorColumn", { bg = c.ui_dim })

  hi("LineNr", { fg = c.ui_bright })
  hi("FoldColumn", { fg = c.ui_bright })
  hi("SignColumn", { fg = c.ui_bright })

  hi("VertSplit", { fg = c.ui })
  hi("StatusLine", { fg = c.text_bright, bg = c.ui })
  hi("StatusLineNC", { fg = c.text, bg = c.ui })
  hi("WinSeparator", { link = "VertSplit" })
  hi("WinBar", { link = "StatusLine" })
  hi("WinBarNC", { link = "StatusLineNC" })
  hi("TabLine", { fg = c.text, bg = c.ui_bright })
  hi("TabLineFill", { fg = c.text, bg = c.ui })
  hi("TabLineSel", { fg = c.ui_dim, bg = c.blue })

  hi("Visual", { fg = c.text_very_bright, bg = c.ui })
  hi("Folded", { fg = c.orange, bg = c.ui_dim })
  hi("QuickFixLine", { bg = blend(c.dim_blue, c.ui_dim, 0.5) })
  hi("qfLineNr", { fg = c.text_dim })
  hi("qfFileName", { fg = c.blue })

  hi("MatchParen", { fg = c.bright_yellow, bold = true })
  hi("Directory", { fg = c.blue })
  hi("IncSearch", { fg = c.bright_orange, reverse = true })
  hi("Search", { fg = c.bright_yellow, reverse = true })

  hi("NonText", { fg = c.ui, bold = true })
  hi("SpecialKey", { fg = c.ui, bold = true })
  hi("Title", { fg = c.bright_blue, bold = true })
  hi("WarningMsg", { fg = c.yellow, bold = true })
  hi("Question", { fg = c.purple, bold = true })
  hi("MoreMsg", { fg = c.blue })
  hi("ModeMsg", { fg = c.green })

  hi("DiffAdd", { bg = c.dim_green })
  hi("DiffChange", { bg = c.dim_yellow })
  hi("DiffDelete", { bg = c.dim_red })
  hi("DiffText", { bg = c.dim_blue })

  hi("SpellBad", { sp = c.error, undercurl = true })
  hi("SpellCap", { sp = c.warning, undercurl = true })
  hi("SpellLocal", { sp = c.info, undercurl = true })
  hi("SpellRare", { sp = c.hint, undercurl = true })

  -- Highlighting
  hi("Comment", { fg = blend(c.magenta, c.ui, 0.8), italic = true })

  hi("Constant", { fg = blend(c.yellow, c.text, 0.8) })
  hi("String", { fg = c.green })
  hi("Character", { fg = c.yellow })
  hi("Number", { fg = c.yellow })
  hi("Boolean", { fg = c.yellow })
  hi("Float", { fg = c.yellow })

  hi("Identifier", { fg = blend(c.blue, c.text, 0.3) })
  hi("Function", { fg = c.blue })

  hi("Statement", { fg = c.purple })
  hi("Conditional", { link = "Statement" })
  hi("Repeat", { link = "Statement" })
  hi("Label", { link = "Statement" })
  hi("Operator", { link = "Statement" })
  hi("Keyword", { link = "Statement" })
  hi("Exception", { link = "Statement" })

  hi("PreProc", { fg = c.magenta })
  hi("Include", { fg = c.magenta })
  hi("Define", { link = "Include" })
  hi("Macro", { link = "Include" })
  hi("PreCondit", { link = "Include" })

  hi("Type", { fg = c.magenta })
  hi("StorageClass", { link = "Type" })
  hi("Structure", { link = "Type" })
  hi("Typedef", { link = "Type" })

  hi("Special", { fg = c.cyan })
  hi("SpecialChar", { link = "Special" })
  hi("Tag", { fg = c.green })
  hi("Delimiter", { fg = c.cyan })
  hi("SpecialComment", { link = "Special" })
  hi("Debug", { fg = c.orange })

  hi("Underlined", { underline = true })
  hi("Ignore", { fg = c.base02 })
  hi("Error", { fg = c.error, bold = true })
  hi("Todo", { fg = c.magenta, bold = true })
  hi("Whitespace", { fg = c.dim_orange })

  -- Identifiers
  hi("@variable", { link = "Identifier" }) -- various variable names
  hi("@variable.builtin", { link = "Special" }) -- built-in variable names (e.g. `this` / `self`)
  -- hi("@variable.parameter", {})  -- parameters of a function
  -- hi("@variable.member", {})  -- object and struct fields

  hi("@constant", { link = "Constant" }) -- constant identifiers
  hi("@constant.builtin", { link = "Special" }) -- built-in constant values
  hi("@constant.macro", { link = "Define" }) -- constants defined by the preprocessor

  hi("@module", {}) -- modules or namespaces
  hi("@module.builtin", {}) -- built-in modules or namespaces
  hi("@label", { link = "Label" }) -- GOTO and other labels (e.g. `label:` in C), including heredoc labels

  -- Literals
  hi("@string", { link = "String" }) -- string literals
  hi("@string.documentation", { fg = blend(c.green, c.ui, 0.7) }) -- string documenting code (e.g. Python docstrings)
  -- hi("@string.regexp", {})  -- regular expressions
  hi("@string.escape", { link = "SpecialChar" }) -- escape sequences
  hi("@string.special", { link = "SpecialChar" }) -- other special strings (e.g. dates)
  -- hi("@string.special.symbol", {})  -- symbols or atoms
  -- hi("@string.special.path", {})  -- filenames
  -- hi("@string.special.url", {})  -- URIs (e.g. hyperlinks)

  hi("@character", { link = "Character" }) -- character literals
  hi("@character.special", { link = "SpecialChar" }) -- special characters (e.g. wildcards)

  hi("@boolean", { link = "Boolean" }) -- boolean literals
  hi("@number", { link = "Number" }) -- numeric literals
  hi("@number.float", { link = "Float" }) -- floating-point number literals

  -- Types
  hi("@type", { link = "Type" }) -- type or class definitions and annotations
  -- hi("@type.builtin", {}) -- built-in types
  hi("@type.definition", { link = "Typedef" }) -- identifiers in type definitions (e.g. `typedef <type> <identifier>` in C)
  -- hi("@type.qualifier", {}) -- type qualifiers (e.g. `const`)

  -- hi("@attribute", {}) -- attribute annotations (e.g. Python decorators)
  hi("@property", { link = "Identifier" }) -- the key in key/value pairs

  -- Functions
  hi("@function", { link = "Function" }) -- function definitions
  hi("@function.builtin", { link = "Special" }) -- built-in functions
  -- hi("@function.call", {}) -- function calls
  hi("@function.macro", { link = "Macro" }) -- preprocessor macros
  -- hi("@function.method", {})  -- method definitions
  -- hi("@function.method.call", {})  -- method calls

  hi("@constructor", { link = "Special" }) -- constructor calls and definitions
  hi("@operator", { link = "Operator" }) -- symbolic operators (e.g. `+` / `*`)

  -- Keyword
  hi("@keyword", { link = "Keyword" }) -- keywords not fitting into specific categories
  -- hi("@keyword.coroutine", {})  -- keywords related to coroutines (e.g. `go` in Go, `async/await` in Python)
  -- hi("@keyword.function", {})  -- keywords that define a function (e.g. `func` in Go, `def` in Python)
  -- hi("@keyword.operator", {})  -- operators that are English words (e.g. `and` / `or`)
  -- hi("@keyword.import", {})  -- keywords for including modules (e.g. `import` / `from` in Python)
  -- hi("@keyword.storage", {})  -- modifiers that affect storage in memory or life-time
  -- hi("@keyword.repeat", {})  -- keywords related to loops (e.g. `for` / `while`)
  -- hi("@keyword.return", {})  -- keywords like `return` and `yield`
  -- hi("@keyword.debug", {})  -- keywords related to debugging
  hi("@keyword.exception", { link = "Exception" }) -- keywords related to exceptions (e.g. `throw` / `catch`)
  hi("@keyword.conditional", { link = "Conditional" }) -- keywords related to conditionals (e.g. `if` / `else`)
  -- hi("@keyword.conditional.ternary", {})  -- ternary operator (e.g. `?` / `:`)
  hi("@keyword.directive", { link = "Define" }) -- various preprocessor directives & shebangs
  hi("@keyword.directive.define", { link = "PreProc" }) -- preprocessor definition directives

  -- Punctuation
  hi("@punctuation.delimiter", { link = "Delimiter" }) -- delimiters (e.g. `;` / `.` / `,`)
  -- hi("@punctuation.bracket", {})  -- brackets (e.g. `()` / `{}` / `[]`)
  -- hi("@punctuation.special", {})  -- special symbols (e.g. `{}` in string interpolation)

  -- Comments
  hi("@comment", { link = "Comment" }) -- line and block comments
  -- hi("@comment.documenting", {})  -- comments documenting code
  hi("@comment.error", { fg = c.error }) -- error-type comments (e.g. `ERROR`, `FIXME`, `DEPRECATED:`)
  hi("@comment.warning", { fg = c.warning }) -- warning-type comments (e.g. `WARNING:`, `FIX:`, `HACK:`)
  hi("@comment.note", { fg = c.info }) -- note-type comments (e.g. `NOTE:`, `INFO:`, `XXX`)
  hi("@comment.todo", { fg = c.hint }) -- todo-type comments (e.g. `TODO:`, `WIP:`, `FIXME:`)

  -- Markup
  hi("@markup.strong", { bold = true }) -- bold text
  hi("@markup.italic", { italic = true }) -- italic text
  hi("@markup.strikethrough", { strikethrough = true }) -- struck-through text
  hi("@markup.underline", { underline = true }) -- underlined text (only for literal underline markup!)
  hi("@markup.heading", { link = "Title" }) -- headings, titles (including markers)
  hi("@markup.heading.1", { fg = c.purple, bold = true }) -- h1
  hi("@markup.heading.2", { fg = c.blue, bold = true }) -- h2
  hi("@markup.heading.3", { fg = c.cyan, bold = true }) -- h3
  hi("@markup.heading.4", { fg = c.green, bold = true }) -- h4
  hi("@markup.heading.5", { fg = c.yellow, bold = true }) -- h5
  hi("@markup.heading.6", { fg = c.orange, bold = true }) -- h6
  hi("@markup.quote", { italic = true }) -- headings, titles (including markers)
  hi("@markup.math", { link = "Special" }) -- math environments (e.g. `$ ... $` in LaTeX)
  hi("@markup.environment", { link = "Macro" }) -- environments (e.g. in LaTeX)
  hi("@markup.environment.name", { link = "Type" })
  hi("@markup.link", { link = "Delimiter" }) -- references, footnotes, citations, etc.
  hi("@markup.link.label", { link = "Function" }) -- link, reference descriptions
  hi("@markup.link.url", { link = "Constant" }) -- URL-style links
  hi("@markup.raw", { link = "Statement" }) -- literal or verbatim text (e.g. inline code)
  -- hi("@markup.raw.block", {}) -- literal or verbatim text as a stand-alone block
  -- --                             (use priority 90 for blocks with injections)
  hi("@markup.list", { link = "Special", bold = true }) -- list markers
  hi("@markup.list.checked", { fg = c.bright_green }) -- checked todo-style list markers
  hi("@markup.list.unchecked", { fg = c.bright_blue }) -- literal or verbatim text (e.g. inline code)

  hi("@function.diff", { link = "Title" }) -- header/filename (for diff files)
  hi("@constant.diff", { link = "Constant" }) -- rev. set (for diff files)
  hi("@attribute.diff", { link = "Special" }) -- line range (for diff files)
  hi("@diff.plus", { link = "diffAdded" }) -- added text (for diff files)
  hi("@diff.minus", { link = "diffRemoved" }) -- deleted text (for diff files)
  hi("@diff.delta", { link = "diffChanged" }) -- changed text (for diff files)

  hi("@tag", { link = "Label" }) -- XML-style tag names (and similar)
  hi("@tag.attribute", { link = "@property" }) -- XML-style tag attributes
  hi("@tag.delimiter", { link = "Delimiter" }) -- XML-style tag delimiters

  hi("@lsp.type.class", { link = "Structure" })
  hi("@lsp.type.comment", { link = "Comment" })
  hi("@lsp.type.decorator", { link = "Function" })
  hi("@lsp.type.enum", { link = "Structure" })
  hi("@lsp.type.enumMember", { link = "Constant" })
  hi("@lsp.type.function", { link = "Function" })
  hi("@lsp.type.interface", { link = "Structure" })
  hi("@lsp.type.macro", { link = "Macro" })
  hi("@lsp.type.method", { link = "Function" })
  hi("@lsp.type.namespace", { link = "Structure" })
  hi("@lsp.type.parameter", { link = "Identifier" })
  hi("@lsp.type.property", { link = "Identifier" })
  hi("@lsp.type.struct", { link = "Structure" })
  hi("@lsp.type.type", { link = "Type" })
  hi("@lsp.type.typeParameter", { link = "Typedef" })
  hi("@lsp.type.variable", { link = "Identifier" })

  -- HTML
  hi("htmlTag", { fg = c.text })
  hi("htmlEndTag", { fg = c.text })

  -- Markdown
  hi("markdownH1", { fg = c.purple, bg = blend(c.dim_purple, c.ui_dim, 0.4) })
  hi("markdownH2", { fg = c.blue, bg = blend(c.dim_blue, c.ui_dim, 0.3) })
  hi("markdownH3", { fg = c.cyan, bg = blend(c.dim_cyan, c.ui_dim, 0.3) })
  hi("markdownH4", { fg = c.green, bg = blend(c.dim_green, c.ui_dim, 0.3) })
  hi("markdownH5", { fg = c.yellow, bg = blend(c.dim_yellow, c.ui_dim, 0.3) })
  hi("markdownH6", { fg = c.orange, bg = blend(c.dim_orange, c.ui_dim, 0.3) })
  hi("markdownCode", { fg = c.purple })

  -- Patch
  hi("diffFile", { fg = c.purple, bold = true })
  hi("diffLine", { fg = c.cyan, bold = true })
  hi("diffAdded", { fg = c.green })
  hi("diffChanged", { fg = c.yellow })
  hi("diffRemoved", { fg = c.red })

  -- Python
  hi("pythonClassVar", { fg = c.cyan })
  hi("pythonExClass", { fg = c.red })

  -- YAML
  hi("yamlKey", { fg = c.blue })

  -- Plugins
  -------------

  -- Gitsigns
  hi("GitSignsAdd", { fg = c.dim_green })
  hi("GitSignsChange", { fg = c.dim_yellow })
  hi("GitSignsDelete", { fg = c.dim_red })
  hi("GitSignsStagedAdd", { fg = blend(c.dim_green, c.bg, 0.5) })
  hi("GitSignsStagedChange", { fg = blend(c.dim_yellow, c.bg, 0.5) })
  hi("GitSignsStagedDelete", { fg = blend(c.dim_red, c.bg, 0.5) })

  -- Illuminate
  hi("IlluminatedWordText", { bg = blend(c.bright_yellow, c.bg, 0.2) })
  hi("IlluminatedWordRead", { bg = blend(c.bright_green, c.bg, 0.2) })
  hi("IlluminatedWordWrite", { bg = blend(c.bright_red, c.bg, 0.2) })

  -- InterestingWord
  hi("InterestingWord1", { fg = c.text_bright, bg = c.dim_blue })
  hi("InterestingWord2", { fg = c.text_bright, bg = c.dim_cyan })
  hi("InterestingWord3", { fg = c.text_bright, bg = c.dim_green })
  hi("InterestingWord4", { fg = c.text_bright, bg = c.dim_orange })
  hi("InterestingWord5", { fg = c.text_bright, bg = c.dim_red })
  hi("InterestingWord6", { fg = c.text_bright, bg = c.dim_magenta })

  -- LSP
  hi("DiagnosticError", { fg = c.error })
  hi("DiagnosticWarn", { fg = c.warning })
  hi("DiagnosticInfo", { fg = c.info })
  hi("DiagnosticHint", { fg = c.hint })
  hi("DiagnosticUnnecessary", { fg = c.bright_cyan })
  hi("DiagnosticVirtualTextError", { link = "DiagnosticError" })
  hi("DiagnosticVirtualTextWarn", { link = "DiagnosticWarn" })
  hi("DiagnosticVirtualTextInfo", { link = "DiagnosticInfo" })
  hi("DiagnosticVirtualTextHint", { link = "DiagnosticHint" })
  hi("DiagnosticUnderlineError", { sp = c.error, undercurl = true })
  hi("DiagnosticUnderlineWarn", { sp = c.warning, undercurl = true })
  hi("DiagnosticUnderlineInfo", { sp = c.info, undercurl = true })
  hi("DiagnosticUnderlineHint", { sp = c.hint, undercurl = true })
  hi("LspReferenceText", { bg = c.ui })
  hi("LspReferenceRead", { bg = c.ui })
  hi("LspReferenceWrite", { bg = c.ui })
  hi("LspSignatureActiveParameter", { bg = c.dim_yellow })
  hi("LspCodeLens", { bg = c.ui_bright })
  hi("LspInlayHint", { bg = c.dim_blue })
  hi("LspInfoBorder", { fg = c.border, bg = c.float_bg })
  hi("DapStoppedLine", { bg = c.bright_yellow })

  -- Lspsaga
  hi("CodeActionNumber", { fg = c.info, bold = true })
  hi("SagaNormal", { link = "NormalFloat" })
  hi("SagaBorder", { link = "FloatBorder" })
  hi("SagaSep", { fg = c.bright_blue })

  -- Lualine
  hi("LuaLineDiffAdd", { link = "diffAdded" })
  hi("LuaLineDiffChange", { link = "diffChanged" })
  hi("LuaLineDiffDelete", { link = "diffRemoved" })
  -- Reload lualine to update its theme.
  -- It automatically generates some highlight groups that are not affected
  -- by the theme defined in "lualine_theme()".
  -- Wrap it with "pcall" because this will only work once Lazy has loaded
  -- the plugin
  pcall(function()
    local ui_plugins = require("stefan.plugins.ui")
    for _, plugin in pairs(ui_plugins) do
      if plugin[1] == "nvim-lualine/lualine.nvim" then
        -- Make bg "transparent" b/c of the rounded caps that I use:
        hi("StatusLine", { bg = nil })
        hi("StatusLineNC", {  bg = nil })
        require("lualine").setup(plugin.opts())
        return
      end
    end
  end)

  -- Neotree
  hi("NeoTreeDimText", { fg = c.text_dim }) -- dim text, expanders, indent markers
  hi("NeoTreeMessage", { fg = c.text_dim, italic = true }) -- num. of hidden files
  hi("NeoTreeDotfile", { fg = c.text_dim }) -- names of hidden files
  hi("NeoTreeModified", { link = "diffChanged" }) -- file as unsaved changes
  hi("NeoTreeGitAdded", { link = "diffAdded" })
  hi("NeoTreeGitModified", { link = "diffChanged" })
  hi("NeoTreeGitDeleted", { link = "diffRemoved" })
  hi("NeoTreeGitUntracked", { fg = c.purple })
  hi("NeoTreeGitIgnored", { link = "NeoTreeDotfile" })
  hi("NeoTreeGitUnstaged", { link = "diffRemoved" })
  hi("NeoTreeGitStaged", { link = "diffAdded" })
  hi("NeoTreeGitConflict", { fg = c.bright_red, bold = true })

  -- Telescope
  hi("TelescopeNormal", { link = "NormalFloat" })
  hi("TelescopeBorder", { link = "FloatBorder" })

  -- Treesitter Context
  hi("TreesitterContextBottom", { sp = c.bright_blue, underline = true })
  hi("TreesitterContextLineNumberBottom", { sp = c.bright_blue, underline = true })
end

--- Return the lualine theme
function M.lualine_theme()
  local c = M.get_colors()
  local style_b = { fg = c.text, bg = c.ui_bright }
  local style_c = { fg = c.text_dim, bg = c.ui }
  return {
    normal = {
      a = { fg = c.blue, bg = c.dim_blue },
      b = style_b,
      c = style_c,
    },
    insert = {
      a = { fg = c.green, bg = c.dim_green },
      b = style_b,
      c = style_c,
    },
    visual = {
      a = { fg = c.magenta, bg = c.dim_magenta },
      b = style_b,
      c = style_c,
    },
    replace = {
      a = { fg = c.orange, bg = c.dim_orange },
      b = style_b,
      c = style_c,
    },
    command = {
      a = { fg = c.yellow, bg = c.dim_yellow },
      b = style_b,
      c = style_c,
    },
    terminal = {
      a = { fg = c.cyan, bg = c.dim_cyan },
      b = style_b,
      c = style_c,
    },
    inactive = {
      a = {},
      b = style_b,
      c = style_c,
    },
  }
end

return M
