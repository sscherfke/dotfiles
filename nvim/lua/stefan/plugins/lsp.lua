-- Install additional Python packages into the venv of a Mason package.
---@param name string Mason package name
---@param pkgs table<string> List of Python packages to install
local function pip_install_extras(name, pkgs)
  local pkg = require("mason-registry").get_package(name)
  pkg:on("install:success", function()
    local function mason_package_path(package)
      local path = vim.fn.resolve(vim.fn.stdpath("data") .. "/mason/packages/" .. package)
      return path
    end
    local path = mason_package_path(name)
    local command = path .. "/venv/bin/pip"
    local args = { "install", "-U", unpack(pkgs) }
    require("plenary.job"):new({ command = command, args = args, cwd = path }):start()
  end)
end

--
-- LSP Configuration & Plugins
--
return {
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "jay-babu/mason-null-ls.nvim",
      "WhoIsSethDaniel/mason-tool-installer.nvim",

      -- Additional lua configuration, makes nvim stuff amazing!
      "folke/neodev.nvim",
    },
    init = function()
      local lspconfig = require("lspconfig")

      local mason = require("mason")
      local mason_lspconfig = require("mason-lspconfig")
      local mason_null_ls = require("mason-null-ls")
      local mason_tool_installer = require("mason-tool-installer")

      mason.setup({
        PATH = "append", -- Give precedence to (Python) tools installed in the current venv
        ui = {
          border = "rounded",
        },
      })

      mason_lspconfig.setup({
        -- list of servers for mason to install
        ensure_installed = {
          "bashls", -- https://github.com/bash-lsp/bash-language-server
          -- "cssls",
          -- "html",
          "lua_ls",
          -- Markdown:
          -- "marksman",
          -- "prosemd_lsp",
          -- "remark_ls",
          -- "zk",
          "jedi_language_server",
          -- "pylsp",
          "ruff",
          -- "rust_analyzer",
          "taplo",
          -- "yamlls",
        },
        -- auto-install configured servers (with lspconfig)
        automatic_installation = true, -- not the same as ensure_installed
      })

      mason_null_ls.setup({
        -- list of formatters & linters for mason to install
        ensure_installed = {
          "mypy",
          "stylua",
        },
        -- auto-install configured formatters & linters (with null-ls)
        automatic_installation = true,
      })
      pip_install_extras("mypy", { "pydantic", "typed-settings" })

      mason_tool_installer.setup({
        ensure_installed = {
          "shellcheck",
          "shfmt",
        },
      })

      -----
      -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
      local cmp_nvim_lsp = require("cmp_nvim_lsp")
      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = cmp_nvim_lsp.default_capabilities(capabilities)

      -- Change the Diagnostic symbols in the sign column (gutter)
      local signs = require("stefan.icons").diagnostics
      for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
      end

      -- See https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua#L404-L449
      -- for an alternative way to setup mason-lspconfig and the individual LSPs
      lspconfig.bashls.setup({ capabilities = capabilities })
      -- lspconfig.jedi_language_server.setup({ capabilities = capabilities })
      lspconfig.taplo.setup({ capabilities = capabilities })

      lspconfig.lua_ls.setup({
        capabilities = capabilities,
        log_level = 4,
        -- Using "settings" ignores workspace ".luarc" config files.
        -- "on_init" can be used to dynamically define "settings", see:
        -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#lua_ls
        on_init = function(client)
          if client.workspace_folders then
            local path = client.workspace_folders[1].name
            if vim.loop.fs_stat(path .. "/.luarc.json") or vim.loop.fs_stat(path .. "/.luarc.jsonc") then
              return
            end
          end

          client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
            diagnostics = { globals = { "vim" }, },
            format = { enable = false },
            runtime = { version = "LuaJIT", },
            -- Make the server aware of Neovim runtime files
            workspace = {
              checkThirdParty = false,
              library = {
                vim.env.VIMRUNTIME,
                vim.fn.stdpath("config") .. "/lua",
              },
            },
          })
        end,
        settings = { Lua = {} },
      })

      lspconfig.jedi_language_server.setup({ capabilities = capabilities })
      -- -- PyLSP
      -- --- PyLSP plugins: Install additional dependencies after pylsp has been installed
      -- --- See: https://github.com/williamboman/mason.nvim/discussions/908
      -- local pylsp = require("mason-registry").get_package("python-lsp-server")
      -- pylsp:on("install:success", function()
      --   local function mason_package_path(package)
      --     local path = vim.fn.resolve(vim.fn.stdpath("data") .. "/mason/packages/" .. package)
      --     return path
      --   end
      --   local path = mason_package_path("python-lsp-server")
      --   local command = path .. "/venv/bin/pip"
      --   local args = { "install", "-U", "pylsp-mypy" }
      --   require("plenary.job"):new({ command = command, args = args, cwd = path }):start()
      -- end)
      -- --- end: PyLSP plugins
      -- lspconfig.pylsp.setup({
      --   capabilities = capabilities,
      --   settings = {
      --     pylsp = {
      --       plugins = {
      --         pylsp_mypy = { enabled = true, live_mode = true },
      --         autopep8 = { enabled = false },
      --         flake8 = { enabled = false },
      --         mccabe = { enabled = false },
      --         pycodestyle = { enabled = false },
      --         pydocstyle = { enabled = false },
      --         pyflakes = { enabled = false },
      --         yapf = { enabled = false },
      --       },
      --     },
      --   },
      -- })
      -- -- end: PyLSP
      lspconfig.ruff.setup({
        init_options = {
          settings = {
            -- https://docs.astral.sh/ruff/editors/settings/
          },
        },
      })
    end,
  },

  -- LSP-like interface for formatters
  {
    "nvimtools/none-ls.nvim",
    dependencies = {
      "williamboman/mason.nvim",
      "nvimtools/none-ls-extras.nvim",
    },
    opts = function()
      local null_ls = require("null-ls")
      -- local code_actions = null_ls.builtins.code_actions
      local formatting = null_ls.builtins.formatting
      local diagnostics = null_ls.builtins.diagnostics

      -- configure null_ls
      return {
        -- setup formatters & linters
        -- diagnostics_format = "[#{c}] #{m} (#{s})",
        sources = {
          -- ...with({ prefer_local = ".venv/bin" }) arg to use executable from local .venv/bin
          --
          -- Code actions
          -- code_actions.gitsigns,  -- Done in LspSaga setup
          -- Diagnostics
          diagnostics.mypy,
          diagnostics.trail_space,
          -- Formatting
          formatting.stylua,
          require("none-ls.formatting.trim_newlines"),
          require("none-ls.formatting.trim_whitespace"),
        },
      }
    end,
  },

  -- Enhanced LSP UIs
  {
    "nvimdev/lspsaga.nvim",
    event = "LspAttach",
    config = function()
      local opts = {
        -- keybinds for navigation in lspsaga window
        scroll_preview = { scroll_down = "<C-f>", scroll_up = "<C-b>" },
        code_action = {
          extend_gitsigns = true,
        },
        definition = {
          edit = "<CR>",
        },
        lightbulb = {
          enable = false,
        },
        symbol_in_winbar = {
          separator = "  ",
        },
        outline = {
          keys = {
            toggle_or_jump = "<space>",
          },
        },
        ui = {
          border = "rounded",
          button = { " ", " " },
        },
      }
      require("lspsaga").setup(opts)
      -- Some highlights can only be changed *after* "setup()" has been called:
      vim.api.nvim_set_hl(0, "SagaActionTitle", { link = "FloatTitle" })

      -- See https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua#L361-L402
      -- for additional bindings
      local function map(mode, lhs, rhs, desc)
        vim.keymap.set(mode, lhs, rhs, { desc = desc, noremap = true, silent = true })
      end
      map("n", "K", "<cmd>Lspsaga hover_doc<cr>", "LSP: Show documentation for what is under cursor")
      map("n", "gd", "<cmd>Lspsaga peek_definition<cr>", "LSP: See [d]efinition and make edits in popup")
      map("n", "gD", "<cmd>Lspsaga goto_definition<cr>", "LSP: See [D]efinition and make edits in window")
      -- map("n", "<leader>ld", "<Cmd>lua vim.lsp.buf.declaration()<cr>", "LSP: Got to declaration")
      -- map("n", "<leader>li", "<cmd>lua vim.lsp.buf.implementation()<cr>", "LSP: Go to implementation")
      map("n", "<leader>lf", "<cmd>Lspsaga lsp_finder<cr>", "LSP: [F]ind definition, references")
      map("n", "<leader>ca", "<cmd>Lspsaga code_action<cr>", "LSP: See available [C]ode [A]ctions")
      map("n", "<leader>rn", "<cmd>Lspsaga rename<cr>", "LSP: [R]e[n]ame word under cursor")
      map("n", "<leader>df", "<cmd>Lspsaga show_buf_diagnostics<cr>", "LSP: Show [d]iagnostics for [f]ile")
      map("n", "<leader>dl", "<cmd>Lspsaga show_line_diagnostics<cr>", "LSP: Show  [d]iagnostics for [l]ine")
      map("n", "<leader>dc", "<cmd>Lspsaga show_cursor_diagnostics<cr>", "LSP: Show [d]iagnostics for [c]ursor")
      map("n", "<leader>dp", "<cmd>Lspsaga diagnostic_jump_prev<cr>", "LSP: Jump to [p]revious [d]iagnostic in buffer")
      map("n", "<leader>dn", "<cmd>Lspsaga diagnostic_jump_next<cr>", "LSP: Jump to [n]ext [d]iagnostic in buffer")
      map("n", "<leader>dco", vim.diagnostic.setqflist, "LSP: Show all [d]iagnostics in quickfix list [co]")
      map("n", "<leader>dlo", vim.diagnostic.setloclist, "LSP: Show buffer [d]iagnostics in location list [ll]")
      map("n", "<leader>o", "<cmd>Lspsaga outline<cr>", "LSP: See [o]utline on right hand side")
      map("n", "<leader>fm", function()
        vim.lsp.buf.format({ async = false })
        vim.lsp.buf.code_action({ context = { only = { "source.fixAll" } }, apply = true })
      end, "LSP: [F]or[m]at current file")
    end,
  },

  -- VS-code like icons for autocompletion
  -- TODO: Manually add icons? https://github.com/hrsh7th/nvim-cmp/wiki/Menu-Appearance
  { "onsails/lspkind.nvim" },
}
